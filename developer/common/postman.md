# Postman

## Postman with Gitlab

Instead of using postman cloud as storage. We will use gitlab as storage for API documentation

File tree in the git will be as following

```
.
├── .postman
│   ├── api
│   └── api_931d529f-e0bd-414e-8e53-c0eb3a0c9901
└── postman
    └── collections
        ├── AffTrackerAPI.json
        ├── M090 local.json
        ├── Mobifreedo API.json
        └── m090 bucuoc.json
```

### Connect

1. Select **APIs** in the left sidebar and select an API.

   ![Screenshot 2023-04-07 at 10.27.05](./assets/postman_sidebar.png)

2. Under **Connect repository**, select **Connect** and select the type of repository you want to connect to.

   ![Screenshot 2023-04-07 at 10.28.17](./assets/postman_connect_repo.png)

3. A browser tab opens asking you to sign in to your repository. Follow the onscreen instructions. When you're finished, close the browser tab and return to Postman.

4. On the **Connect your repository** page, enter the **Organization** or **Workspace** and the **Repository** where the API will be stored. (For GitLab, enter the **Group** and **Project** for your API.)

   ![Connecting to a cloud-hosted repo](./assets/postman_connect.jpg)

   

### Branching & Commit

Similar to VSCode and IntelliJ work flow

- Switch and create branches

  ![Switching branches](./assets/postman_switch_branch.jpg)

- Commit pane (right side bar)

  ![Source control pane](./assets/postman_commit_pane.jpg)

## Use Variables with Postman

Must use variables for following information:

- SERVICE1_BASE_URI
- SERVICE2_BASE_URI
- ROLE1_ACCESS_TOKEN
- ROLE1_USERNAME
- ROLE1_PASSWORD
- ROLE2_ACCESS_TOKEN
- ROLE2_USERNAME
- ROLE2_PASSWORD

#### Set Variable

Store variables in `Postman Collection`  because storing in `Postman Environment` will be difficult to share

![Screenshot 2023-04-07 at 12.45.49](./assets/postman_set_var.png)

#### Use Variable

Use variable anywhere with `{{VAR_HERE}}`

![Screenshot 2023-04-07 at 12.46.03](./assets/postman_use_var.png)

#### Dynamic variables

| **`$guid`**            | A `uuid-v4` style guid                | `"611c2e81-2ccb-42d8-9ddc-2d0bfa65c1b4"` |
| ---------------------- | ------------------------------------- | ---------------------------------------- |
| **`$timestamp`**       | The current UNIX timestamp in seconds | `1562757107`, `1562757108`, `1562757109` |
| **`$isoTimestamp`**    | The current ISO timestamp at zero UTC | `2020-06-09T21:10:36.177Z`               |
| **`$randomUUID`**      | A random 36-character UUID            | `"6929bb52-3ab2-448a-9796-d6480ecad36b"` |
| **`$randomIP`**        | A random IPv4 address                 | `241.102.234.100`, `216.7.27.38`         |
| **`$randomFirstName`** | A random first name                   | `Ethan`, `Chandler`, `Megane`            |
| **`$randomLastName`**  | A random last name                    | `Schaden`, `Schneider`, `Willms`         |

All variables: https://learning.postman.com/docs/writing-scripts/script-references/variables-list/

## Use Script with Postman

Script in Postman Request can be used for long flow request

For example:

1. Sign up using dynamic variables above and store username/password in variables
2. Login API using above variables and it return a token and user id
3. Store user id and token in variable
4. Use token in subsequent requests

![Screenshot 2023-04-07 at 12.47.04](./assets/postman_script.png)

```javascript
pm.test("Settoken", function () {
    var jsonData = pm.response.json();
    pm.environment.set("ROLE1_ACCESS_TOKEN", jsonData.access_token);
});
pm.test("Response time is less than 200ms", function () {
    pm.expect(pm.response.responseTime).to.be.below(200);
});
pm.test("Content-Type is present", function () {
    pm.response.to.have.header("Content-Type");
});
pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});
```

## References

https://learning.postman.com/docs/designing-and-developing-your-api/versioning-an-api/using-external-git-repo/

https://learning.postman.com/docs/designing-and-developing-your-api/versioning-an-api/managing-git-changes/