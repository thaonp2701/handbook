# API

## GraphQL

TODO

## GRPC

TODO

## HTTP API

#### Field naming convention

Frontend/JS friendly 
- camelCase
- No shorthand

#### Value convention

- ??

- ISO 8601 date time

  | Example                | Explanation                                                  |
  | :--------------------- | :----------------------------------------------------------- |
  | 2019-09-07T-15:50+00   | 3:30 pm on September 7, 2019 in the time zone of universal time |
  | 2019-09-07T15:50+00Z   | Likewise, 3:30 pm on September 7, 2019 in the time zone of universal time with the addition of “Z” in the notation |
  | 2019-09-07T15:50-04:00 | 3:30 pm on September 7, 2019 in the time zone New York (UTC with daylight saving time) |


### Restful & Resourceful rules

#### Plural resources

| HTTP Verb | Path             | Controller#Action | Used for                                     |
| :-------- | :--------------- | :---------------- | :------------------------------------------- |
| GET       | /photos          | photos#index      | display a list of all photos                 |
| GET       | /photos/new      | photos#new        | return an HTML form for creating a new photo |
| POST      | /photos          | photos#create     | create a new photo                           |
| GET       | /photos/:id      | photos#show       | display a specific photo                     |
| GET       | /photos/:id/edit | photos#edit       | return an HTML form for editing a photo      |
| PATCH/PUT | /photos/:id      | photos#update     | update a specific photo                      |
| DELETE    | /photos/:id      | photos#destroy    | delete a specific photo                      |

#### Non-plural resource

| HTTP Verb | Path           | Controller#Action | Used for                                      |
| :-------- | :------------- | :---------------- | :-------------------------------------------- |
| GET       | /geocoder/new  | geocoders#new     | return an HTML form for creating the geocoder |
| POST      | /geocoder      | geocoders#create  | create the new geocoder                       |
| GET       | /geocoder      | geocoders#show    | display the one and only geocoder resource    |
| GET       | /geocoder/edit | geocoders#edit    | return an HTML form for editing the geocoder  |
| PATCH/PUT | /geocoder      | geocoders#update  | update the one and only geocoder resource     |
| DELETE    | /geocoder      | geocoders#destroy | delete the geocoder resource                  |

#### Nested resource

| HTTP Verb | Path                                 | Controller#Action | Used for                                                     |
| :-------- | :----------------------------------- | :---------------- | :----------------------------------------------------------- |
| GET       | /magazines/:magazine_id/ads          | ads#index         | display a list of all ads for a specific magazine            |
| GET       | /magazines/:magazine_id/ads/new      | ads#new           | return an HTML form for creating a new ad belonging to a specific magazine |
| POST      | /magazines/:magazine_id/ads          | ads#create        | create a new ad belonging to a specific magazine             |
| GET       | /magazines/:magazine_id/ads/:id      | ads#show          | display a specific ad belonging to a specific magazine       |
| GET       | /magazines/:magazine_id/ads/:id/edit | ads#edit          | return an HTML form for editing an ad belonging to a specific magazine |
| PATCH/PUT | /magazines/:magazine_id/ads/:id      | ads#update        | update a specific ad belonging to a specific magazine        |
| DELETE    | /magazines/:magazine_id/ads/:id      | ads#destroy       | delete a specific ad belonging to a specific magazine        |

#### Shallow nested resource

| HTTP Verb | Path                                         | Controller#Action |
| :-------- | :------------------------------------------- | :---------------- |
| GET       | /articles/:article_id/comments(.:format)     | comments#index    |
| POST      | /articles/:article_id/comments(.:format)     | comments#create   |
| GET       | /articles/:article_id/comments/new(.:format) | comments#new      |
| GET       | /comments/:id/edit(.:format)                 | comments#edit     |
| GET       | /comments/:id(.:format)                      | comments#show     |
| PATCH/PUT | /comments/:id(.:format)                      | comments#update   |
| DELETE    | /comments/:id(.:format)                      | comments#destroy  |
| GET       | /articles/:article_id/quotes(.:format)       | quotes#index      |
| POST      | /articles/:article_id/quotes(.:format)       | quotes#create     |
| GET       | /articles/:article_id/quotes/new(.:format)   | quotes#new        |
| GET       | /quotes/:id/edit(.:format)                   | quotes#edit       |
| GET       | /quotes/:id(.:format)                        | quotes#show       |
| PATCH/PUT | /quotes/:id(.:format)                        | quotes#update     |
| DELETE    | /quotes/:id(.:format)                        | quotes#destroy    |
| GET       | /articles/:article_id/drafts(.:format)       | drafts#index      |
| POST      | /articles/:article_id/drafts(.:format)       | drafts#create     |
| GET       | /articles/:article_id/drafts/new(.:format)   | drafts#new        |
| GET       | /drafts/:id/edit(.:format)                   | drafts#edit       |
| GET       | /drafts/:id(.:format)                        | drafts#show       |
| PATCH/PUT | /drafts/:id(.:format)                        | drafts#update     |
| DELETE    | /drafts/:id(.:format)                        | drafts#destroy    |
| GET       | /articles(.:format)                          | articles#index    |
| POST      | /articles(.:format)                          | articles#create   |
| GET       | /articles/new(.:format)                      | articles#new      |
| GET       | /articles/:id/edit(.:format)                 | articles#edit     |
| GET       | /articles/:id(.:format)                      | articles#show     |
| PATCH/PUT | /articles/:id(.:format)                      | articles#update   |
| DELETE    | /articles/:id(.:format)                      | articles#destroy  |
