# Naming convention

## By programming language

|                     | Python       | C#                    | TypeScript           | Java                 |
| :------------------ | :----------- | :-------------------- | :------------------- | :------------------- |
| functions & methods | snake_case() | PascalCase()          | camelCase()          | camelCase()          |
| classes             | PascalCase   | PascalCase            | PascalCase           | PascalCase           |
| interfaces          | N/A          | PascalCase            | PascalCase           | PascalCase           |
| namespaces          | N/A          | PascalCase            | PascalCase           | PascalCase           |
| constants           | snake_case   | SCREAMING*SNAKE\*CASE | SCREAMING_SNAKE_CASE | SCREAMING_SNAKE_CASE |

## Standard abbreviation

Sử dụng các từ viết tắt tiêu chuẩn (standard abbreviations) để giảm thiểu độ dài của tên biến và phương thức, ví dụ: str, int, max, min, avg.
TODO: Làm rõ list các standard abbreviation