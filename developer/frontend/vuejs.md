# VueJS

## Project structure

### NuxtJS

### Vue

## Coding standards

1. Naming conventions: Use consistent naming conventions for components, methods, data properties, and computed properties. For example, use PascalCase for component names and camelCase for method and data property names.
2. Formatting: Use consistent formatting for your code, including indentation, line breaks, and whitespace. This can help make your code more readable and maintainable.
3. Documentation: Include comments and documentation in your code to explain how it works, what it does, and why it is necessary. This can make it easier for other developers to understand and modify your code.
4. Error handling: Handle errors gracefully by using try-catch blocks, error messages, and appropriate error codes. This can help prevent crashes and improve the user experience.

Example of above standards

```vue
<template>
  <div>
    <h1>{{ title }}</h1>
    <p>{{ description }}</p>
  </div>
</template>

<script>
export default {
  name: 'MyComponent',

  data() {
    return {
      title: 'Hello, world!',
      description: 'This is an example Vue.js component.',
    }
  },

  methods: {
    handleClick() {
      console.log('Button clicked!')
    },
  },

  computed: {
    message() {
      return `The title is "${this.title}" and the description is "${this.description}".`
    },
  },
}
</script>

<style>
/* CSS styles go here */
</style>

```

## Best practices/patterns

- Global error handling for API calls
- Modal must be local component with Vue Teleport
- Avoid state management at all cost
- RxJS for back-pressure batch processing
- Use router-guard for global concerns
- If possible to get OpenAPI (swagger) file, use typescript generator for type safe api call
- https://vueuse.org
- https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd

## References

- 
