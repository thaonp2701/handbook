# YCOMMVN Handbook

We need rules and conventions, so we will document everything to help everyone onboard efficiently

Please note that this is public repository and open for contribution, DO NOT PUT ANY CONFIDENTIAL INFORMATION here

## Target Audiences

- [X] Business Analyst
- [X] UI/UX
- [X] Developer
- [X] QC

## Contributing

### Guideline

- [Use Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [Use mermaid diagram](https://about.gitlab.com/handbook/tools-and-tips/mermaid/)
- Use VSCode's Typora extension for editor
