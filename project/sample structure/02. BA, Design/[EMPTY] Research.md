# Introduction

`<Summary insight knowledge after research, analysis>`

# Documents

| No | Name               | Link                     | Version |
| -- | ------------------ | ------------------------ | ------- |
| 1  | Competior Analysis | [Competitor Analysis.xls]() | 1.3     |
| 2  | Value Proposition  | [Value Proposition.pptx]()  | 1.2     |
| 3  | Market Research    | [Marktet Research.docx]()   | 1       |
| 4  | UX Research        | [Figma]()                   | 1       |
| 5  | Value Mapping      | [Value Mapping.xls]()       | 1       |
| 6  | Wireframe          | [Figma]()                   | 1       |
| 7  | ......             |                          | 1       |
