
# Introduction

<Input description, introduction of project here>

[Client&#39;s Document]()

# Hierarchy Module

[Hierarchy Module.xls]()

# Timeline

Short description about important milestones of the project

[WBS.xls]()

# Members

| No | Name        | Position |
| -- | ----------- | -------- |
| 1  | Le Duc X    | PL       |
| 2  | Chu Quang Y | PM       |
| 3  | .....       | ....     |

# Documents

| No | Name                  | Link            | Version |
| -- | --------------------- | --------------- | ------- |
| 1  | Storyboard            | [Storyboard 1.3]() | 1.3     |
| 2  | Storyboard            | [Storyboard 1.2]() | 1.2     |
| 3  | Testcase              | [Testcase]()       | 1       |
| 4  | UI Design             | [Figma]()          | 1       |
| 5  | Backend Architecture  | [Backend]()        | 1       |
| 6  | Frontend Architecture | [Frontend]()       | 1       |
