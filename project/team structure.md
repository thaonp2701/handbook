# Role matrix

## Introduction

|               | Matrix                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Advantages    | - Tối ưu hoá nguồn lực, có thể thực hiện nhiều dự án khác nhau cùng lúc với các vai trò khác nhau.<br />Tối ưu hơn so với cách phân bổ người theo dự án (người giỏi ít việc hoặc không thể hiện hết khả năng)<br />- Truyền tải thông tin theo chiều ngang chiều dọc tốt hơn (giữa các thành viên với nhau, giữa các tầng quản lý các bộ phận)<br />- Dự án được hỗ trợ nhiều hơn từ các phía chuyên môn chức năng<br />- Dự án vận hành tốt hơn so với cách phân chia cấu trúc theo team chức năng<br />- Có khả năng mở rộng tốt trong tương lai, cơ hội rộng mở cho nhiều vị trí<br />- Mục tiêu dự án rõ ràng hơn |
| Disadvantages | - Tiềm ẩn nhiều mâu thuẫn về trách nhiệm vai trò<br />- Sự phân bổ thành viên vai trò trong dự án phức tạp hơn<br />- Sẽ có nhiều yêu cầu phải tuân thủ hơn để thực hiện đúng quy trình<br />- Thành viên sẽ có nhiều hơn một quản lý<br />- Phức tạp hơn trong việc quản lý và vận hành                                                                                                                                                                                                                                                                                                                                                                                         |

## **Sample role matrix assignment**

### Generic skill

|  Name  | PM | Engineering Lead | BA | BA Team Facilitator | UI/UX | UI/UX Team Facilitator | QC | QC Team Facilitator | Deveveloper | Developer Team Facilitator |
| :----: | :-: | :--------------: | :-: | :-----------------: | ----- | :---------------------: | -- | :-----------------: | :---------: | :------------------------: |
| Name 1 | x |                  |    |                    |       |                        |    |                    |            |                            |
| Name 2 |    |                  | x |          x          |       |            x            |    |          x          |            |                            |
| Name 3 |    |        x        |    |                    |       |                        |    |                    |      x      |             x             |

### Specialized skill

|  Name  | PM | Engineering Lead | BA | BA Team Facilitator | UI/UX | UI/UX Team Facilitator | QC | QC Team Facilitator | Deveveloper | Developer Team Facilitator |
| :----: | :-: | :--------------: | :-: | :-----------------: | ----- | :---------------------: | -- | :-----------------: | :---------: | :------------------------: |
| Name 1 | x |                  |    |                    |       |                        |    |                    |            |                            |
| Name 2 |    |                  | x |          x          |       |            x            |    |          x          |            |                            |
| Name 3 |    |        x        |    |                    |       |                        |    |                    |            |                            |
| Name 5 |    |                  |    |                    |       |                        |    |                    |     BE     |             BE             |
| Name 6 |    |                  |    |                    |       |                        |    |                    |     FE     |             FE             |
| Name 7 |    |                  |    |                    |       |                        |    |                    |     ML     |             ML             |
| Name 8 |    |                  |    |                    |       |                        |    |                    |     FE     |                            |
| Name 9 |    |                  |    |                    |       |                        |    |                    |     BE     |                            |

## Roles & Responsibilities (R&R)

### PM

1. **Chịu trách nhiệm** dự án, phải quản lý, điều phối theo dõi quá trình thực hiện dự án: quality, technical, planing, risk. Đặt lợi ích của dự án lên đầu tiên.
2. Xác định, làm rõ mục tiêu, đảm bảo tiến độ giao hàng, xác định rủi do cùng các bên liên quan **(cần viết tài liệu WBS, Hierarchy Module, Risk Management, Proposal)**
3. Tạo kế hoạch, triển khai và theo dõi tiến độ kế hoạch cùng với các thành viên, bên liên quan để thực hiện dự án **(cần viết tài liệu Initiation Report, Planing Report, Closing Report, Timeline)**
4. Quyết định cuối về đánh giá sản phẩm, tiến độ, phạm vị dự án đã mang lại đúng giá trị được yêu cầu.
5. Host các buổi meeting / communication
   1. Giới thiệu mục tiêu, kế hoạch dự án, xác định rủi do (**Kickoff Meeting, Initiation Meeting**)
   2. Đánh giá tiến độ, sản phẩm (**Sprint Review**)
   3. Quản lý tiến độ tập trung vào ngăn chặn rủi do, xử lý vấn đề, chất lượng dự án (**Sprint Review, Scheduled Meeting, Risk Idenity Meeting**)
   4. Đánh giá cải tiến, thay đổi quy trình thực hiện dự án cho phù hợp với các bên liên quan (Client, BA, QA, Dev,...) (**Restrospective**)

### BA

1. Phân tích, nghiên cứu xây dựng sản phẩm có giá trị hoặc yêu cầu khách hàng **(WBS, Hierachy Module, Proposal, Market Research, Competior Analysis, UX Research, User Journey Map, Value Mapping, etc,...)**
2. Vai trò chính xây dựng tài liệu yêu cầu chức năng chi tiết - **(Storyboard, SRS, Wireframe)**
3. Trao đổi, quản lý giao tiếp với khách hàng, thành viên và các bên liên quan để đảm bảo xây dựng đúng sản phẩm mang lại giá trị
4. Phối hợp với các bên liên quan để thực hiện tài liệu cuối dự án **(User Guide - Installation, Operation)**
5. Chủ động báo cáo các vấn đề, rủi do gây ảnh hưởng tới mục tiêu của dự án
6. Host các buổi meeting / communication
   1. Dev, UI/UX Design, QC(giải thích yêu cầu chức năng, ước lượng thời gian hoàn thành -  **Sprint Planning**)
   2. UI/UX Design (xây dựng giải pháp tính năng - **Feature, Design Planning**)
   3. PM/Lead hoặc toàn bộ các bên liên quan (chốt phương án tính năng, scope, thời gian)

#### BA Team Facilitator (2h/ngày)

1. Trách nhiệm trả lời đầu tiên về yêu cầu chức năng của sản phẩm, vấn đề chung của dự án, giao tiếp các bên liên quan xử lý vấn đề, lỗi phía người dùng sản phẩm
2. Quyết định cuối cùng về chức năng sản phẩm đúng yêu cầu mục tiêu và mang lại giá trị
3. Chia task UI/UX
4. Host các buổi meeting / communication
   1. BA - UI/UX Design - Lên kế hoạch xây dựng giải pháp tính năng (**Features Planning, Product Planning, Design Planning**)

### UI/UX

1. Chịu trách nhiệm chính về thực hiện tài liệu UX và UI theo yêu cầu của dự án **(UX Research, User Persona, User Journey Map, Design Guide, UI Kit, Moodboard, Screenshot, Concept Design, Visualization, Animation,....)**
2. Quản lý sắp xếp các nguồn resource UX/UI. Tất cả các tệp nguồn phải được chia sẻ và truy cập bởi cả nhóm
3. Thực hiện nghiên cứu thị trường, đối thủ cạnh tranh và người dùng để tìm cảm hứng và giải quyết các vấn đề (**UX Research, Competior Analysis, Value Proposition, Wireframe**)
4. Hiểu toàn bộ mục đích của ứng dụng đang được phát triển, phối hợp với các bên liên quan để đưa ra giải pháp đúng yêu cầu và cân bằng trong các hoàn cảnh khác nhau
5. Chủ động báo cáo các vấn đề, rủi do gây ảnh hưởng tới mục tiêu của dự án

#### UI/UX Team Facilitator (2h/ngày)

1. Trách nhiệm trả lời đầu tiên về UI/UX của sản phẩm, các vấn đề feedback của user về tính năng hay hành vi người dùng
2. Quyết định cuối về giao diện người dùng, hành vi tính năng sản phẩm
3. Chia task UI/UX
4. Host các buổi meeting / communication
   1. BA - UI/UX Design (xây dựng giải pháp tính năng - **Feature, Design Planning**)
   2. PM/BA/Engineering Lead (chốt giải pháp giao diện, hành vi tính năng sản phẩm) (**Design Review**)

### QC

1. Thực hiên các tài liệu về testing (**Test case, Test report, Test plan,...**)
2. Thực hiện các phương pháp testing để đảm bảo chất lượng sản phẩm từ phía đội phát triển (Manual, Automation, UI, API, Database testing)
3. Phối hợp với các bên liên quan để thực hiện tài liệu cuối dự án **(User Guide)**
4. Chủ động báo cáo các vấn đề, rủi do gây ảnh hưởng tới mục tiêu của dự án

#### QC Team Facilitator (2h/ngày)

1. Trách nhiệm trả lời đầu tiên về các lỗi tính năng phía người dùng/Client
2. Quyết định cuối về chất lượng sản phẩm đã đủ tốt và đúng yêu cầu hay chưa
3. Chia task cho QC
4. Host các buổi meeting / communication
   1. QC - xây dựng kế hoạch testing sản phẩm - **Testing Planing**
   2. Các bên liên quan - **Quality** **Review Meeting, Incident Analysis Meeting**

### Developer

1. Thực hiện các tài liệu và phát triển kỹ thuật toàn bộ sản phẩm dự án (**Backend/Frontend Document, Deployment Document, ERD, API Document,...** )
2. Tham gia vào xây dựng, quyết định kỹ thuật của dự án (**Architecture Design System**)
3. Phối hợp các bên liên quan để xác định rủi do, và thời gian thực hiện nhiệm vụ
4. Chủ động báo cáo các vấn đề, rủi do gây ảnh hưởng tới mục tiêu của dự án

#### Developer Team Facilitator (2h/ngày)

1. Trách nhiệm trả lời đầu tiên về các vấn đề kỹ thuật và lỗi kỹ thuật phía người dùng/Client
2. Quyết định phương án kỹ thuật thực hiện tính năng sản phẩm
3. Review code, Merge pull request
4. Chia task cho developer
5. Host các buổi meeting / communication
   1. Developer - Xây dựng giải pháp kỹ thuật - **Technical Meeting**
   2. Engineeering Lead - Xây dựng giải pháp kiến trúc - **Architecture Meeting**

### Engineering Lead (1h/ngày)

1. **Chịu trách nhiệm** chính về kỹ thuật, kiến trúc công nghệ của dự án
2. Duyệt và kiểm tra các tài liệu tính chính xác và đúng đắn các tài liệu kỹ thuật của dự án
3. Quyết định cuối về kỹ thuật, kiến trúc chung của dự án
4. Host các buổi meeting / communication
   1. Developer - Xây dựng giải pháp kỹ thuật, kiến trúc - **Technical Meeting, Architecture Meeting**
